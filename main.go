package main

import (
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"embed"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"image/png"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"github.com/alexmullins/zip"
	goflags "github.com/jessevdk/go-flags"
	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
)

//go:generate rsrc -manifest main.manifest -ico favicon.ico -o rsrc.syso
//go:embed alta.png success.png ok.png fail.png favicon.png
var content embed.FS

var logFile *os.File

func log(format string, args ...interface{}) {
	x := fmt.Sprintf(format, args...)
	fmt.Println(x)
	if logFile != nil {
		logFile.WriteString(x)
		logFile.WriteString("\r\n")
	}
}

func dopanic(merr error) {
	log(merr.Error())
	if logFile != nil {
		logFile.Close()
	}
	os.Exit(1)
}

type okError struct {
	txt string
}

func (e *okError) Error() string {
	return e.txt
}
func makeOkError(x string, y ...interface{}) error {
	return &okError{fmt.Sprintf(x, y...)}
}
func isOkError(err error) bool {
	_, ok := err.(*okError)
	return ok
}

func assetImg(name string) walk.Image {
	data, err := content.Open(name)
	if err != nil {
		dopanic(err)
	}
	logo, err := png.Decode(data)
	if err != nil {
		dopanic(err)
	}
	img, err := walk.NewBitmapFromImageForDPI(logo, 96)
	if err != nil {
		dopanic(fmt.Errorf("walk from %T %s", logo, err.Error()))
	}
	return img
}

func main() {
	var opts struct {
		Verbose string `short:"v" long:"verbose" description:"verbose"`
		Cloud   string `short:"c" long:"cloud" description:"Alta Cloud to use" default:"https://aware.avasecurity.com"`
	}

	dir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	f, err := os.Create(path.Join(dir, "exportVerifier.log"))
	if err == nil {
		logFile = f
		defer logFile.Close()
	}

	remaining, err := goflags.NewParser(&opts, goflags.HelpFlag|goflags.PassAfterNonOption).Parse()
	if err != nil {
		dopanic(err)
	}

	var w *walk.MainWindow
	var lbl1 *walk.Label
	var lbl2 *walk.TextLabel
	var logoImgView *walk.ImageView
	var resultImgView *walk.ImageView

	favicon := assetImg("favicon.png")
	sz := Size{500, 500}
	mw := MainWindow{
		Title:   "Alta Video Export Verifier",
		Icon:    favicon,
		Size:    sz,
		MaxSize: sz,
		MinSize: sz,
		Layout:  VBox{},
		Children: []Widget{
			ImageView{
				AssignTo: &logoImgView,
				Image:    assetImg("alta.png"),
				MaxSize:  Size{500, 500},
			},
			Label{
				AssignTo:      &lbl1,
				Text:          "Alta Video Export Validator",
				MinSize:       Size{Width: 0, Height: 100},
				TextAlignment: AlignCenter,
				Font: Font{
					PointSize: 24,
				},
			},
			ImageView{
				Visible:   false,
				AssignTo:  &resultImgView,
				Alignment: AlignHCenterVNear,
				Mode:      ImageViewModeShrink,
			},
			TextLabel{
				AssignTo:           &lbl2,
				Text:               "",
				TextAlignment:      AlignHCenterVCenter,
				AlwaysConsumeSpace: true,
				Font: Font{
					PointSize: 12,
				},
			},
			PushButton{
				Text: "Close",
				Font: Font{
					PointSize: 18,
				},
				MaxSize: Size{100, 40},
				Column:  100,
				OnClicked: func() {
					w.Close()
				},
			},
		},
		AssignTo: &w,
	}
	err = mw.Create()
	if err != nil {
		dopanic(err)
	}

	go func() {
		time.Sleep(time.Second)

		var file string
		if len(remaining) > 0 {
			file = remaining[0]
		} else {
			pathChan := make(chan string)

			w.Synchronize(func() {
				dlg := new(walk.FileDialog)
				dlg.Filter = "ZIP Files (*.zip)|*.zip"
				dlg.Title = "Select an export file from Alta Aware"

				if ok, err := dlg.ShowOpen(w); err != nil {
					dopanic(err)
				} else if !ok {
					w.Close()
					return
				}
				pathChan <- dlg.FilePath
			})

			file = <-pathChan
		}

		lbl1.SetText("Processing file")

		passCallback := func() string {
			passChan := make(chan string)
			w.Synchronize(func() {
				var edit *walk.LineEdit
				var dlg *walk.Dialog
				var pass string
				_, err := Dialog{
					AssignTo: &dlg,
					Title:    "Enter Password",
					Size:     Size{200, 80},
					Layout:   HBox{},
					Icon:     favicon,
					Children: []Widget{
						LineEdit{
							OnKeyPress: func(key walk.Key) {
								if key == walk.KeyReturn {
									dlg.Accept()
								}
							},
							AssignTo:     &edit,
							PasswordMode: true,
							OnTextChanged: func() {
								pass = edit.Text()
							},
							MinSize: Size{160, 70},
						},
						PushButton{
							Text: "OK",
							OnClicked: func() {
								dlg.Accept()
							},
						},
					},
				}.Run(w)
				if err != nil {
					dopanic(err)
				}
				if pass == "" {
					dopanic(fmt.Errorf("No password supplied"))
				}
				passChan <- pass
			})
			return <-passChan
		}

		setResult := func(s string) {
			resultImgView.SetImage(assetImg(s))
			resultImgView.SetVisible(true)
		}
		setText := func(s string) {
			const MAX_LINE = 50
			ret := ""
			for len(s) > MAX_LINE {
				i := MAX_LINE
				for ; i > 0 && s[i] != ' '; i-- {
				}
				if i > 0 {
					ret = ret + s[:i] + "\n"
					s = s[i:]
				}
			}
			ret = ret + s
			lbl2.SetText(ret)
		}

		_, err := doVerify(file, opts.Cloud, passCallback)
		if err != nil {
			if isOkError(err) {
				lbl1.SetText("Unverified")
				lbl1.SetTextColor(walk.RGB(255, 128, 39))
				setText(err.Error())
				setResult("ok.png")
			} else {
				lbl1.SetText("Failed")
				lbl1.SetTextColor(walk.RGB(255, 0, 0))
				setText(err.Error())
				setResult("fail.png")
			}
		} else {
			lbl1.SetText("Success")
			lbl1.SetTextColor(walk.RGB(0, 255, 0))
			setText(fmt.Sprintf("Successfully validated for Alta Aware"))
			setResult("success.png")
		}
	}()

	w.Run()
	os.Exit(0)
}

func doVerify(file string, cloud string, passWdCallback func() string) (string, error) {
	serial := ""

	z, err := zip.OpenReader(file)
	if err != nil {
		return serial, err
	}

	files := make(map[string]*zip.File)
	for _, f := range z.File {
		files[f.Name] = f
	}

	pass := ""

	openFile := func(f *zip.File) (io.Reader, error) {
		for {
			if pass != "" {
				f.SetPassword(pass)
			}
			r, err := f.Open()
			if err != nil {
				if err == zip.ErrPassword {
					if pass != "" {
						return nil, fmt.Errorf("Failed to decrypt, incorrect password")
					}
					pass = passWdCallback()
					continue
				}
			}
			return r, err
		}
	}

	certFile, ok := files["SIGN/certificate.pem"]
	if !ok {
		return serial, fmt.Errorf("Missing certificate file in ZIP")
	}
	certFileHandle, err := openFile(certFile)
	if err != nil {
		return serial, err
	}
	certFileData, err := ioutil.ReadAll(certFileHandle)
	if err != nil {
		return serial, fmt.Errorf("Unable to read certificate file: %s", err.Error())
	}

	block, _ := pem.Decode(certFileData)
	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return serial, fmt.Errorf("Unable to parse certificate: %s", err.Error())
	}
	serial = cert.Subject.CommonName

	var verifier func(hash []byte, signature []byte) error
	switch pub := cert.PublicKey.(type) {
	case *rsa.PublicKey:
		verifier = func(hash []byte, signature []byte) error {
			return rsa.VerifyPKCS1v15(pub, crypto.SHA256, hash, signature)
		}
	default:
		return serial, fmt.Errorf("Unsupported certificate type %T", cert.PublicKey)
	}

	totalFiles := 0
	failedFiles := 0
	for fname, f := range files {
		if !strings.HasPrefix(fname, "SIGN/") {
			signatureFile, ok := files["SIGN/"+fname+".sig"]
			if !ok {
				return serial, fmt.Errorf("Unable to find matching signature file for '%s'", fname)
			}
			signatureFileHandle, err := openFile(signatureFile)
			if err != nil {
				return serial, fmt.Errorf("Unable to open '%s' in zip: %s", signatureFile.Name, err)
			}
			signatureFileData, err := ioutil.ReadAll(signatureFileHandle)
			if err != nil {
				return serial, fmt.Errorf("Unable to read '%s' in zip: %s", signatureFile.Name, err)
			}

			hasher := sha256.New()
			fhandle, err := openFile(f)
			if err != nil {
				return serial, fmt.Errorf("Unable to open '%s' in zip: %s", fname, err)
			}
			_, err = io.Copy(hasher, fhandle)
			if err != nil {
				return serial, fmt.Errorf("Unable to hash '%s': %s", fname, err)
			}
			hash := hasher.Sum(nil)

			err = verifier(hash[:], signatureFileData)
			if err != nil {
				failedFiles++
			}
			totalFiles++
		}
	}
	if failedFiles > 0 {
		return serial, fmt.Errorf("%d/%d files failed to validate", failedFiles, totalFiles)
	}

	certFileHash := sha256.Sum256(certFileData)

	client := http.Client{
		Transport: &http.Transport{
			DialContext: (&net.Dialer{
				Timeout: 5 * time.Second,
			}).DialContext,
			TLSHandshakeTimeout: 5 * time.Second,
		},
	}

	certificateHash := base64.StdEncoding.EncodeToString([]byte(certFileHash[:]))
	log("Verifying '%s' '%s'", serial, certificateHash)
	query := fmt.Sprintf("%s/api/v1/public/verifyServerCertificate?serial=%s&certificateHash=%s", cloud, strings.ToLower(serial), url.QueryEscape(certificateHash))

	req, err := http.NewRequest("GET", query, nil)
	if err != nil {
		return serial, makeOkError("Error creating http request: %s", err.Error())
	}
	log("%s", query)

	res, err := client.Do(req)
	if err != nil {
		return serial, makeOkError("Error talking to cloud to verify certificate: %s", err.Error())
	}
	defer res.Body.Close()

	if res.StatusCode >= 300 {
		body, _ := ioutil.ReadAll(res.Body)
		log("Got status code %d, content %s", res.StatusCode, string(body))
		return serial, makeOkError("All files validated but certificate could not be verified for '%s'", serial)
	}
	log("Success")

	return serial, nil
}
