module gitlab.com/vaion/exportVerifier

go 1.16

require (
	github.com/akavel/rsrc v0.8.0 // indirect
	github.com/alexmullins/zip v0.0.0-20180717182244-4affb64b04d0
	github.com/jessevdk/go-flags v1.4.0
	github.com/jteeuwen/go-bindata v3.0.7+incompatible // indirect
	github.com/lxn/walk v0.0.0-20210112085537-c389da54e794
	github.com/lxn/win v0.0.0-20210218163916-a377121e959e // indirect
	github.com/zserge/webview v0.0.0-20191103184548-1a9ebffc2601
	golang.org/x/crypto v0.0.0-20191105034135-c7e5f84aec59 // indirect
	gopkg.in/Knetic/govaluate.v3 v3.0.0 // indirect
)
