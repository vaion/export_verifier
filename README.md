#### Alta Video Export File Verifier

###### Run Instructions

- Export a video file (zip) from an Alta **Aware** system
- Drag and drop the video file onto the export verifier 

###### Build Requirements

- Install Go 1.18+

###### Build Instructions

- `GOOS=windows GOARCH=amd64 go build -ldflags="-H windowsgui"`